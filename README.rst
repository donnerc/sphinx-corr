##############################
Extension sphinx - corrections
##############################

Ce dépôt contient le code-source d'une extension sphinx codée en ``javascript`` qui permet de sélectionner le contenu d'un ``code-block``.

Pour ajouter cette extension à un projet sphinx existant, veuillez suivre les étapes suivantes:

* Créez un dossier ``_static`` dans le répertoire ``./sources``
* Ajoutez-y le fichier ``corr.js`` de ce dépôt
* Copiez le fichier ``__init___.py`` dans ``venv/lib/python3.4/site-packages/custom/corr/``
* Ajoutez ``custom.corr`` dans la liste ``extensions`` de ``conf.py``

* Implémentez la directive ``corr``::

	..  corr::
	
		Ce texte est masqué. Pour l'afficher, il faut cliquer sur le bouton "corrigé".
		
		..  note::
		
			Il est tout à fait possible d'inclure d'autres extensions rst dans cette extension.

Démonstration
=============

.. figure:: demo/hidden.png
.. figure:: demo/showed.png